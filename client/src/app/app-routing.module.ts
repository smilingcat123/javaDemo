import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/auth/login/login.component';
import { LogoutComponent } from './components/auth/logout/logout.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { CreateAccountComponent } from './components/auth/create-account/create-account.component';
import { ProductsComponent } from './components/products/products.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { PaymentComponent } from './components/payment/payment.component';
import { OrderConfirmComponent } from './components/order-confirm/order-confirm.component';

const routes: Routes = [
  {
    path: '',
    component: HomepageComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'creataccount',
    component: CreateAccountComponent
  },
  {
    path: 'products',
    component: ProductsComponent
  },
  {
    path: 'detail/:pro_id',
    component: ProductDetailComponent
  },
  {
    path: 'checkout',
    component: CheckoutComponent
  },
  {
    path: 'payment',
    component: PaymentComponent
  },
  {
    path: 'confirm',
    component: OrderConfirmComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
