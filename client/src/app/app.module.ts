import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/auth/login/login.component';
import { LogoutComponent } from './components/auth/logout/logout.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { CreateAccountComponent } from './components/auth/create-account/create-account.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { NgxGalleryModule } from 'ngx-gallery';


import { AuthService } from './shared/service/auth.service';
import { ProductsService} from './shared/service/products.service';
import { OrdersService} from './shared/service/orders.service';

import { HttpModule} from '@angular/http';
import { ProductsComponent } from './components/products/products.component';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { PaymentComponent } from './components/payment/payment.component';
import { OrderConfirmComponent } from './components/order-confirm/order-confirm.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LogoutComponent,
    HeaderComponent,
    FooterComponent,
    HomepageComponent,
    CreateAccountComponent,
    ProductsComponent,
    ProductDetailComponent,
    CheckoutComponent,
    PaymentComponent,
    OrderConfirmComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    NgxGalleryModule
  ],
  providers: [
    AuthService,
    ProductsService,
    OrdersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
