import { Injectable} from '@angular/core';
import {users} from './shared/users';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

// observable: an object which continously an asynchronously emit to all its observers over time.

@Injectable()
export class UserService {

  userURL = 'http://localhost:3000/users';


  constructor(
    private http: Http
  ) {}
  getUsers(): Observable<users[]> {
    return this.http.get(this.userURL)
      .map(res => res.json() as users[]);
  }

  getUser(id: number): Observable<users> {
    return this.http.get(this.userURL + '/' + id)
      .map(res => res.json() as users);
  }

  postUsers(login_email: string, login_pass: string): Observable<users> {
    console.log(login_email, login_pass);
    return this.http.post(this.userURL, {login_email, login_pass})
      .map(res => res.json() as users);
  }



}
