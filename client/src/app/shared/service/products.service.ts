import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {Products} from '../Products';
import {AppConfig} from "./app.config";


@Injectable()
export class ProductsService {

  private API_URL = AppConfig.API_URL;
 // private API_URL = "http://localhost:8080/products";

  constructor(
    private http: Http
  ) { }



  getProducts(): Observable<Products[]> {
    return this.http.get(this.API_URL + '/products')
      .map(res => res.json() as Products[]);
  }

  getProduct(pro_id: number): Observable<Products> {
    console.log(pro_id);
    return this.http.get(this.API_URL + '/products/' + pro_id)
      .map(res => res.json() as Products);
  }

}
