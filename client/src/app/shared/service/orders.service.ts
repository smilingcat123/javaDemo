import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject} from 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';
import { Http, HttpModule,URLSearchParams} from '@angular/http';
import {AppConfig} from "./app.config";
import { Router} from '@angular/router';

@Injectable()
export class OrdersService {

  private API_URL = AppConfig.API_URL;
  public totalItems = new BehaviorSubject<number>(0);
  orderDetail;

  constructor(
    private http: Http,
    private router: Router
  ) { }

  addItems(num: number) {
    console.log(num);
    let oldvalue = +this.totalItems.getValue();
    this.totalItems.next(oldvalue + num);
  }

  placeOrder(values): Observable<any> {
    console.log(values);
    return this.http.post(this.API_URL + "/orders",values)
      .map(res => res.json())
      .map((res) => {
          console.log("22222");
          return res;
      });
  }

  sendCode(user): Observable<any> {
    console.log(user);
    //this.simpleStringify(values);
    return this.http.post(this.API_URL + "/sendcode",user)
      .map(res => res.json())
      .map((res) => {
        console.log("22222");
        return res;
      });
  }

  orderConfirm(value) {
    console.log(value);
    this.orderDetail = value;
    this.totalItems.next(0);
    this.router.navigate(['/confirm']);
  }

}
