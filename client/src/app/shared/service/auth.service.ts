import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject} from 'rxjs/Rx';
import { Http, HttpModule,URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {users} from '../users';
import {AppConfig} from "./app.config";


@Injectable()
export class AuthService {

  private API_URL = AppConfig.API_URL;
  //userURL = 'http://localhost:3000/users';
  //private API_URL = "http://localhost:8080";
  public loggedIn: Subject<boolean> = new BehaviorSubject<boolean>(false);
  currentUser: string = "guest";


  constructor(
    private http: Http
  ) { }

  logIn(vaules): Observable<{success}> {
    return this.http.post(this.API_URL + "/users",vaules)
      .map(res => res.json() as {success});
  }

  newlog(user): Observable<any> {
    console.log(user);
    let params = new URLSearchParams();
    params.set("username", user.username);
    params.set("password", user.password);
    console.log(params);
    return this.http.post(this.API_URL + "/login", params, {withCredentials: true})
      .map(res => res.json())
      .map((res) => {
       // this.loggedIn.next(res.success);
        this.currentUser = user.username;
        return res;
      });
  }


  createAccount(vaules): Observable<users> {
    return this.http.put(this.API_URL + "/users",vaules)
      .map(res => res.json() as users);
  }

  register(user): Observable<any> {
    return this.http.post(this.API_URL + "/register", user)
      .map(res => res.json());
  }


  login() {
    this.loggedIn.next(true);
  }

  logout() {
    this.loggedIn.next(false);
  }



}
