export class Products {
  pro_ID: number;
  pro_SRC: String;
  pro_TITLE: String;
  pro_RATING: String;
  pro_PRICE: number;
  pro_TOTAL: number;
}
