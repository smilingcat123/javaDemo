import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../../shared/service/orders.service';

@Component({
  selector: 'app-order-confirm',
  templateUrl: './order-confirm.component.html',
  styleUrls: ['./order-confirm.component.scss']
})
export class OrderConfirmComponent implements OnInit {
  order;

  constructor(
    private ordersService: OrdersService
  ) { }

  ngOnInit() {
    this.order = this.ordersService.orderDetail;
  }

}
