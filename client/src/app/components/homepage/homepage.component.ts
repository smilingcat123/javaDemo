import { Component, OnInit } from '@angular/core';
import 'hammerjs/hammer';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';


@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  constructor() { }

  ngOnInit() {
    this.galleryOptions = [
      {
        // width: '600px',
        // height: '400px',
        width: '1000px',
        height: '428.56px',
        //fullWidth: true,
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide
      },
      {
        width: '1000px',
        height: '428.56px',
        thumbnails: false,
        "imageAutoPlay": true,
        "imageAutoPlayPauseOnHover": true,
        "previewAutoPlay": true,
        "previewAutoPlayPauseOnHover": true
      },
      // max-width 800
      {
        breakpoint: 800,
        width: '100%',
        height: '600px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      // max-width 400
      {
        breakpoint: 400,
        preview: false
      }
    ];

    this.galleryImages = [
      {
        small: 'https://s3.us-east-2.amazonaws.com/msi-final-st/hc-food__95762.jpg',
        medium: 'https://s3.us-east-2.amazonaws.com/msi-final-st/hc-food__95762.jpg',
        big: 'https://s3.us-east-2.amazonaws.com/msi-final-st/hc-food__95762.jpg'
      },
      {
       small: 'https://cdn6.bigcommerce.com/s-ih7nl5lym4/product_images/theme_images/hc-giftideas__56957.jpg',
        medium: 'https://cdn6.bigcommerce.com/s-ih7nl5lym4/product_images/theme_images/hc-giftideas__56957.jpg',
        big: 'https://cdn6.bigcommerce.com/s-ih7nl5lym4/product_images/theme_images/hc-giftideas__56957.jpg'
      },
      {
        small: 'https://cdn6.bigcommerce.com/s-ih7nl5lym4/product_images/theme_images/HC-Blog.jpg',
        medium: 'https://cdn6.bigcommerce.com/s-ih7nl5lym4/product_images/theme_images/HC-Blog.jpg',
        big: 'https://cdn6.bigcommerce.com/s-ih7nl5lym4/product_images/theme_images/HC-Blog.jpg'
      },
      {
        small: 'https://cdn6.bigcommerce.com/s-ih7nl5lym4/templates/__custom/images/Store01.jpg',
        medium: 'https://cdn6.bigcommerce.com/s-ih7nl5lym4/templates/__custom/images/Store01.jpg',
        big: 'https://cdn6.bigcommerce.com/s-ih7nl5lym4/templates/__custom/images/Store01.jpg'
      },
      {
        small: 'https://static1.squarespace.com/static/545d4f45e4b0b49a35dd989e/57e6e2af29687f1c49bf66a6/57e6e346ff7c5055bd06dc25/1477546018335/Sushi+Roll1.jpg',
        medium: 'https://static1.squarespace.com/static/545d4f45e4b0b49a35dd989e/57e6e2af29687f1c49bf66a6/57e6e346ff7c5055bd06dc25/14775' +
        '46018335/Sushi+Roll1.jpg',
        big: 'https://static1.squarespace.com/static/545d4f45e4b0b49a35dd989e/57e6e2af29687f1c49bf66a6/57e6e346ff7c5055bd06dc25/1477546018335/Sushi+Roll1.jpg'
      }

    ];
  }
}
