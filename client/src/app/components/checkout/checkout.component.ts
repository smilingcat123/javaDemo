import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/service/auth.service';
import {forEach} from "@angular/router/src/utils/collection";
import { OrdersService } from '../../shared/service/orders.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  information;
  constructor(
    private authService: AuthService,
    private ordersService: OrdersService
  ) { }

  ngOnInit() {
    this.information = JSON.parse(localStorage.getItem(this.authService.currentUser));
    console.log(this.information);
  }

  getTotal() {
    let total = 0;
    //forEach()
    for (let product of this.information) {
        //total += this.information[i].PTY * this.information[i].PRO_PRICE;
        total = total + (+product.PTY * +product.PRO_PRICE);
        console.log(total);
    }
    return total;
  }

  removeProduct(value) {
    console.log("click me");
    console.log(value);
    let num = 0;
    this.information = this.information.filter(function(emp) {
      if (emp.PRO_ID == value) {
        num = num + emp.PTY;
        return false;
      }
      return true;
    });
    localStorage.setItem(this.authService.currentUser, JSON.stringify(this.information));
    console.log(localStorage.getItem(this.authService.currentUser));

    let oldvalue = +this.ordersService.totalItems.getValue();
    this.ordersService.totalItems.next(oldvalue - num);

  }
}
