import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Location} from '@angular/common';
import { Products} from '../../shared/Products';
import { ProductsService} from '../../shared/service/products.service';
import { OrdersService } from '../../shared/service/orders.service';
import { AuthService } from '../../shared/service/auth.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  selProduct: Products;
  qty: number;

  constructor(
    private route: ActivatedRoute,
    private productsService: ProductsService,
    private location: Location,
    private ordersService: OrdersService,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.route.paramMap
      .subscribe((paramMap: ParamMap) => {
        console.log(paramMap.get('pro_id'));
        // this.hdHero = this.heroService.getHero(+paramMap.get('id'));
        this.productsService.getProduct(+paramMap.get('pro_id'))
          .subscribe((product: Products) => {
            this.selProduct = product;
            console.log(this.selProduct);
          });

      });
    localStorage.getItem(this.authService.currentUser);
    console.log(localStorage.getItem(this.authService.currentUser));
  }

  addToCart(value) {
    console.log(value.qty);
    console.log(this.selProduct.pro_ID);
    this.ordersService.addItems(+value.qty);
    let data =
      {
      "PRO_ID":this.selProduct.pro_ID,
      "USER":this.authService.currentUser,
      "PTY":+value.qty,
      "PRO_PRICE":+this.selProduct.pro_PRICE,
      "PRO_SRC":this.selProduct.pro_SRC,
      "PRO_TITLE":this.selProduct.pro_TITLE
    }

    console.log(this.authService.currentUser);
    console.log(localStorage.getItem(this.authService.currentUser));

    let oldData = JSON.parse(localStorage.getItem(this.authService.currentUser)) || [];

    // if(typeof this.authService.currentUser == "undefined"){
    //   if(typeof oldData == "undefined") {
    //     oldData = new Array();
    //     oldData.push(data);
    //     localStorage.setItem("guest", JSON.stringify(oldData));
    //     console.log(localStorage.getItem("guest"));
    //   }else{
    //     oldData.push(data);
    //     localStorage.setItem("guest", JSON.stringify(oldData));
    //     console.log(localStorage.getItem("guest"));
    //   }
    //   //localStorage.setItem("guest", JSON.stringify(data));
    // }

      if(typeof oldData == "undefined"){
        oldData = new Array();
        oldData.push(data);
        localStorage.setItem(this.authService.currentUser, JSON.stringify(oldData));
        console.log(localStorage.getItem(this.authService.currentUser));
      }else{
        oldData.push(data);
        localStorage.setItem(this.authService.currentUser, JSON.stringify(oldData));
        console.log(localStorage.getItem(this.authService.currentUser));
      }


  }

}
