import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/service/auth.service';
import { OrdersService } from '../../shared/service/orders.service';
import { Router} from '@angular/router';


@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  information;
  discount = 1;
  checkEmail = false;
  checkFirstname = false;
  checkLastname = false;
  checkZip = false;
  checkCity = false;
  checkState = false;
  checkStreet = false;
  checkPhone = false;
  errors;

  constructor(
    private authService: AuthService,
    private ordersService: OrdersService,
    private router: Router
  ) { }

  ngOnInit() {
    this.information = JSON.parse(localStorage.getItem(this.authService.currentUser));
    console.log(this.information);
  }

  getTotal() {
    let total = 0;
    //forEach()
    for (let product of this.information) {
      //total += this.information[i].PTY * this.information[i].PRO_PRICE;
      total = total + (+product.PTY * +product.PRO_PRICE);
      console.log(total);
    }
    return total;
  }

  getDiscount(value){
    console.log(value);
    let code = {
      coupon: "LIURUI"
    }
    let code1 = {
      coupon: "YIYAO"
    }
    let code2 = {
      coupon: "ROBERT"
    }
    if(JSON.stringify(value) == JSON.stringify(code)){
      return this.discount = 0.85;
    }else if(JSON.stringify(value) == JSON.stringify(code1)){
      return this.discount = 0.5;
    }else if(JSON.stringify(value) == JSON.stringify(code2)) {
      return this.discount = 0;
    }else
    return this.discount;
  }


  checkout(value){
    console.log(value.lastName);
    console.log(value.firstName);
    console.log(value.zip);


    let address = value.streetAddress + " " + value.city + " " + value.state + " "  + value.zip;
    let total = (this.getTotal() * this.discount) * 1.0825;
    let userDetail = {
      "user_id": this.authService.currentUser,
      "user_email": value.email,
      "first_name": value.firstName,
      "last_name": value.lastName,
      "user_address": address,
      "user_phone": value.phone,
      "user_card_number": value.cardNumber,
      "user_card_date": value.expirationDate,
      "user_card_cvc": value.cardCVC
    }
    let d = new Date();
    let newId = d.getTime();
    let currentDate =  (d.getMonth() + 1)+d.getDate() + d.getFullYear();
    let numberItem = this.ordersService.totalItems.getValue();
    let orderDetail = {
      "id": newId,
      //"qty": this.ordersService.totalItems,
      //"qty":numberItem,
      "email": value.email,
      "address": address,
      "total": total.toFixed(2)
    }





      this.ordersService.placeOrder(orderDetail).subscribe(res => {
        console.log(res);
        // this.router.navigate(['/confirm']);
      });
      this.ordersService.orderConfirm(orderDetail);



  }

  validateUser(user){

    if(this.validateString(user.firstName)) {
      this.checkFirstname = true;
    }else{
      this.checkFirstname = false;
    }

    if(this.validateString(user.lastName)) {
      this.checkLastname = true;
    }else{
      this.checkLastname = false;
    }

    if(this.validUSZip(user.zip)) {
      this.checkZip = false;
    }else{
      this.checkZip = true;
    }

    if(this.validateString(user.city)) {
      this.checkCity = true;
    }else{
      this.checkCity = false;
    }

    if(this.validateString(user.streetAddress)) {
      this.checkStreet = true;
    }else{
      this.checkStreet = false;
    }

    if(this.phonenumber(user.phone)) {
      this.checkPhone = false;
    }else{
      this.checkPhone = true;
    }

    if(this.validateString(user.state)) {
      this.checkState = true;
    }else{
      this.checkState = false;
    }



  }



  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validatePassword(str)
  {
    // at least one number, one lowercase and one uppercase letter
    // at least six characters
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    return re.test(str);
  }

  validateString(str) {
    if(str.length == 0) {
      return true;
    }else return false;
  }


  validUSZip(sZip) {
    return /^\d{5}(-\d{4})?$/.test(sZip);
  }

  phonenumber(inputtxt) {
    var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    if(inputtxt.value.match(phoneno)) {
      return true;
    }
    else {
      return false;
    }
  }


}
