import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup} from '@angular/forms';
import { Observable } from 'rxjs/Rx';

import {ActivatedRoute, ParamMap} from "@angular/router";
import {Location} from '@angular/common';

import { AuthService } from '../../../shared/service/auth.service';
import { Router} from '@angular/router';

import { users } from '../../../shared/users'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private loginForm: FormGroup;
  private route: ActivatedRoute;
  response;
  checkEmail = false;
  checkPassword = false;


  constructor(
    private authService: AuthService,
    private router: Router,
    private location: Location
  ) {
    this.loginForm = new FormGroup({
      login_email: new FormControl('example@gmail.com'),
      login_pass: new FormControl()
    });
  }

  ngOnInit() {
    this.route.paramMap
      .subscribe((paramMap: ParamMap) => {
        console.log(paramMap.get('id'));
        // this.hdHero = this.heroService.getHero(+paramMap.get('id'));
      });
  }

  newlogin(user) {
    console.log(user.username);
    console.log(user.password);
    console.log(this.validateEmail(user.username));
    this.checkUser(user);
    console.log(this.checkEmail);
    console.log(this.checkPassword);
  if(!this.checkEmail && !this.checkPassword) {
    this.authService.newlog(user)
      .subscribe((res) => {
        this.response = res;
        if(res.success){
          console.log("success log in!");
          this.authService.login();
          //this.location.back();
          this.router.navigate(['/']);
          // this.authService.loggedIn.subscribe(res => {})
          //create the localstorage after the user loggedin and the key is the username
          localStorage.setItem(user.username, null);
        }else{
          console.log("fail log in!");
          this.checkEmail = true;
        }
      });
  }

  }

  checkUser(user) {
    if(!this.validateEmail(user.username)){
      this.checkEmail = true;
    }else
    {
      this.checkEmail = false;
    }

    if(this.validatePass(user.firstname)) {
      this.checkPassword = true;
    }else{
      this.checkPassword = false;
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validatePass(pass) {
    if(pass != null) {
      return true;
    }else return false;
  }

  getUsers() {
    this.authService.login();
  }

  login() {
    //this.loading = true;
    this.authService.login();
   // this.router.navigate(['/']);
  }

}
