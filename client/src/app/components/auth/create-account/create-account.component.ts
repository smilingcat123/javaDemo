import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup} from '@angular/forms';
import {Location} from '@angular/common';
import { AuthService } from '../../../shared/service/auth.service';
import { Router} from '@angular/router';
import { OrdersService } from '../../../shared/service/orders.service';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {

  private creatForm: FormGroup;
  checkEmail = false;
  checkPassword = false;
  checkFirstname = false;
  checkLastname = false;
  doubleCheckpass = false;
  multiUser = false;
  errors;

  constructor(
    private authService: AuthService,
    private ordersService: OrdersService,
    private location: Location,
    private router: Router
  ) {
    this.creatForm = new FormGroup({
      login_email: new FormControl('example@gmail.com'),
      login_pass: new FormControl()
    })
  }

  ngOnInit() {
  }

  CreateAccount() {
    console.log(this.creatForm.value);
    this.authService.createAccount(this.creatForm.value)
      .subscribe((res) => { console.log(res);});
   this.location.back();
  }

  register(user) {
    //do the validation
    console.log(user.username);
    console.log(user.password);
    console.log(user.firstname);
    console.log(user.lastname);

    this.validateUser(user);
    if(!this.checkEmail && !this.checkPassword && !this.checkFirstname && !this.checkLastname && !this.doubleCheckpass)
    {
      this.authService.register(user)
        .subscribe((res) => {
          if(res.success){
            this.ordersService.sendCode(user).subscribe(res => {
              console.log(res);
            });
            this.router.navigate(['/login']);
          }
        }, (err) => {
          console.log(err.status);
          if(err.status == 500){
            this.multiUser = true;
          }
        });
    }else{
      console.log("false");
    }



    // this.authService.register(user)
    //   .subscribe((res) => {
    //     if(res.success){
    //       this.ordersService.sendCode(user).subscribe(res => {
    //         console.log(res);
    //       });
    //       this.router.navigate(['/login']);
    //     }
    //   });
  }

  validateUser(user){
    if(!this.validateEmail(user.username)){
      this.checkEmail = true;
    }else
    {
      this.checkEmail = false;
    }
    if(!this.validatePassword(user.password)) {
      this.errors = alert("The password at least six characters, At least one number, one lowercase and one uppercase letter.");
      this.checkPassword = true;
    }else{
      this.errors = [];
      this.checkPassword = false;
    }

    if(this.validateString(user.firstname)) {
      this.checkFirstname = true;
    }else{
      this.checkFirstname = false;
    }

    if(this.validateString(user.lastname)) {
      this.checkLastname = true;
    }else{
      this.checkLastname = false;
    }

    if(this.doubleCheckPassword(user.password, user.confirmPass)) {
      this.doubleCheckpass = true;
    }else{
      this.doubleCheckpass = false;
    }


  }



  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validatePassword(str)
  {
    // at least one number, one lowercase and one uppercase letter
    // at least six characters
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    return re.test(str);
  }

  validateString(str) {
    if(str.length == 0) {
      return true;
    }else return false;
  }

  doubleCheckPassword(oldpass, newpass) {
    if(oldpass != newpass){
        return true;
    }else return false;
  }



}
