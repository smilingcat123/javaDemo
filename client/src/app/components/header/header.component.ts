import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/service/auth.service';
import { OrdersService } from '../../shared/service/orders.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  checker: boolean;
  total: string;

  //localStorage.getItem(this.authService.currentUser).length();

  constructor(
    private authService: AuthService,
    private ordersService: OrdersService
  ) {
  }

  ngOnInit() {
    this.authService.loggedIn.subscribe(checker => this.checker = checker);
    console.log(this.checker);
    this.total = localStorage.getItem("total");
  }

  logout() {
    this.authService.logout();
    this.ordersService.totalItems.next(0);
  }

}
