import { Component, OnInit } from '@angular/core';
import { ProductsService} from '../../shared/service/products.service';
import { Products} from '../../shared/Products';
import { Router} from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  private products: Products[];
  selProduct: Products;

  constructor(
    private productsService: ProductsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.productsService.getProducts()
      .subscribe((products: Products[]) => {
        console.log(products);
        this.products = products;
      });
  }

  goDetail(product: Products) {
    this.selProduct = product;
    console.log(this.selProduct);
    this.router.navigate(['/detail', this.selProduct.pro_ID]);
  }

  sortList(event){
    console.log(event);
    switch (event){
      case "alphaasc":
        this.products.sort((a:any, b:any) => {
            let left = String(a.pro_TITLE);
            let right = String(b.pro_TITLE);
            return left.localeCompare(right);
          }
        );
        break;
      case "alphadesc":
        this.products.sort((a:any, b:any) => {
            let left = String(a.pro_TITLE);
            let right = String(b.pro_TITLE);
            return left.localeCompare(right);
          }
        ).reverse();
        break;
      case "priceasc":
        console.log(this.products);
        this.products.sort((a:any, b:any) => {
            let left = Number(a.pro_PRICE);
            let right = Number(b.pro_PRICE);
            return left - right;
          }
        );
        console.log(this.products);
        break;
      case "pricedesc":
        console.log(this.products);
        this.products.sort((a:any, b:any) => {
            let left = Number(a.pro_PRICE);
            let right = Number(b.pro_PRICE);
            return right - left;
          }
        );
        console.log(this.products);
        break;
      case "avgcustomerreview":
        this.products.sort((a:any, b:any) => {
            let left = Number(a.pro_SCORE);
            let right = Number(b.pro_SCORE);
            return right - left;
          }
        );
        break;
    }
  }
}
