package com.mercury.SpringBootRESTDemo.service;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;


@Service
public class MailService {

	public void sendMail(String email, String title, String body) {
		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		Properties prop = System.getProperties();
		prop.setProperty("mail.smtp.host", "smtp.gmail.com");
		prop.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		prop.setProperty("mail.smtp.socketFactory.fallback", "false");
		prop.setProperty("mail.smtp.port", "465");
		prop.setProperty("mail.smtp.socketFactory.port", "465");
		prop.put("mail.smtp.auth", "true");
		final String user = "openemr555@gmail.com";
		final String password = "St83619113";
        Session session = Session.getInstance(prop, new Authenticator() {
        	protected PasswordAuthentication getPasswordAuthentication() {
        		return new PasswordAuthentication(user, password);
        	}
        });
        try {
        	Message msg = new MimeMessage(session);
        	msg.setFrom(new InternetAddress("openemr555@gmail.com"));
        	msg.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
        	msg.setSubject(title); 
        	msg.setSentDate(new Date());
        	msg.setText(body);
        	msg.setContent(body,"text/html");
        	Transport.send(msg);
        	System.out.println("Message sent succesfully!");
        } catch (Exception e) {
        	System.out.println(e);
        }
	}

}
