package com.mercury.SpringBootRESTDemo.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "msi_product")
public class Product {

	@Id
	@GeneratedValue
	private int PRO_ID;
	@Column
	private String PRO_TITLE;
	@Column
	private String PRO_BRAND;
	@Column
	private String PRO_SRC;
	@Column
	private String PRO_RATING;
	@Column
	private int PRO_PRICE;
	@Column
	private int PRO_TOTAL;
	@Column
	private int PRO_SCORE;
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Product(int pRO_ID, String pRO_TITLE, String pRO_BRAND, String pRO_SRC, String pRO_RATING, int pRO_PRICE,
			int pRO_TOTAL, int pRO_SCORE) {
		PRO_ID = pRO_ID;
		PRO_TITLE = pRO_TITLE;
		PRO_BRAND = pRO_BRAND;
		PRO_SRC = pRO_SRC;
		PRO_RATING = pRO_RATING;
		PRO_PRICE = pRO_PRICE;
		PRO_TOTAL = pRO_TOTAL;
		PRO_SCORE = pRO_SCORE;
	}
	public int getPRO_ID() {
		return PRO_ID;
	}
	public void setPRO_ID(int pRO_ID) {
		PRO_ID = pRO_ID;
	}
	public String getPRO_TITLE() {
		return PRO_TITLE;
	}
	public void setPRO_TITLE(String pRO_TITLE) {
		PRO_TITLE = pRO_TITLE;
	}
	public String getPRO_BRAND() {
		return PRO_BRAND;
	}
	public void setPRO_BRAND(String pRO_BRAND) {
		PRO_BRAND = pRO_BRAND;
	}
	public String getPRO_SRC() {
		return PRO_SRC;
	}
	public void setPRO_SRC(String pRO_SRC) {
		PRO_SRC = pRO_SRC;
	}
	public String getPRO_RATING() {
		return PRO_RATING;
	}
	public void setPRO_RATING(String pRO_RATING) {
		PRO_RATING = pRO_RATING;
	}
	public int getPRO_PRICE() {
		return PRO_PRICE;
	}
	public void setPRO_PRICE(int pRO_PRICE) {
		PRO_PRICE = pRO_PRICE;
	}
	public int getPRO_TOTAL() {
		return PRO_TOTAL;
	}
	public void setPRO_TOTAL(int pRO_TOTAL) {
		PRO_TOTAL = pRO_TOTAL;
	}
	public int getPRO_SCORE() {
		return PRO_SCORE;
	}
	public void setPRO_SCORE(int pRO_SCORE) {
		PRO_SCORE = pRO_SCORE;
	}
	@Override
	public String toString() {
		return "Product [PRO_ID=" + PRO_ID + ", PRO_TITLE=" + PRO_TITLE + ", PRO_BRAND=" + PRO_BRAND + ", PRO_SRC="
				+ PRO_SRC + ", PRO_RATING=" + PRO_RATING + ", PRO_PRICE=" + PRO_PRICE + ", PRO_TOTAL=" + PRO_TOTAL
				+ ", PRO_SCORE=" + PRO_SCORE + "]";
	}

}
