package com.mercury.SpringBootRESTDemo.service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mercury.SpringBootRESTDemo.bean.OrderProduct;
import com.mercury.SpringBootRESTDemo.bean.User;
import com.mercury.SpringBootRESTDemo.bean.UserProfile;
import com.mercury.SpringBootRESTDemo.dao.OrderDao;
import com.mercury.SpringBootRESTDemo.dao.UserDao;
import com.mercury.SpringBootRESTDemo.http.Response;

@Service
@Transactional
public class OrderService {
	@Autowired
	UserDao userDao;
	
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	MailService mailService;
	
	public Response placeOrder(OrderProduct order) {
		// TODO: validation.
		System.out.println(order.toString());
		String emailAddress = order.getEmail();
		System.out.println(emailAddress);
		String title = new String("Your Order Confirmation");
		String  body0 ="<h2 color=\"red\">Thank You For Your Order!</h2>";
		String  body1 ="<hr>";
		String  body2 ="<b>Here Is Your Order Number:" + order.getId() + "</b>";
		
		String  body4 ="<hr>";
		String  body4_1 ="<h2>Your Purchase Information</h2>";
		String  body4_2 ="<p><b>Email Address: "+emailAddress+"</b></p>";
		String  body4_3 ="<p><b>Your Order Number: "+order.getId()+"</b></p>";
		String  body4_4 ="<p><b>Order Total: $ "+order.getTotal()+"</b></p>";
		
		String  body5 ="<hr>";
		String  body5_1 ="<h2>Shipped To: </h2>";
		String  body5_2 ="<p><b>Address: "+order.getAddress()+"</b></p>";
				
		String  body6 ="<p>If you have any questions regarding your account, click 'Reply' in your email client and we'll be only too happy to help.</p>";
		String  body7 ="<b>MTC Kitchen</b>";
		String  body8 ="<p>Japanese Chef Knives, Japanese Tableware and Kitchenware </p>";
		String  body9 ="<hr>";
		String  body10 = "<p>© 2017 MTC Kitchen<p>";
		String  body = body0 + body1 + body2 + body4 + body4_1 + body4_2 + body4_3 +
				body4_4 + body5 + body5_1 + body5_2 + body6 + body7 + body8 +
				body9 + body10;
		mailService.sendMail(emailAddress, title, body);
		return new Response(true);
	}
	
	public Response sendCode(User user) {
		System.out.println(user.toString());
		String emailAddress = user.getUsername();
		String name = user.getFirstname();
		String title = new String("Thanks for Registering at MTC Kitchen");
		String  body0 ="<h2 color=\"red\">Thanks for Registering at MTC Kitchen</h2>";
		String  body1 ="<hr>";
		String  body2 ="<b>Dear  "+name+"</b>";
		String  body3 ="<p>Thank you for creating your account at MTC Kitchen. Your account details are as follows:</p>";
		String  body4 ="<p><b>Email Address: "+emailAddress+"</b></p>";
		String  body5 ="<p><b>Name: "+name+"</b></p>";
		String  body5_1 ="<p><b>First order 15% off!<b><p>";
		String  body5_2 ="<p>Use code: <b>LIURUI<b><p>";
		String  body6 ="<p>If you have any questions regarding your account, click 'Reply' in your email client and we'll be only too happy to help.</p>";
		String  body7 ="<b>MTC Kitchen</b>";
		String  body8 ="<p>Japanese Chef Knives, Japanese Tableware and Kitchenware </p>";
		String  body9 ="<hr>";
		String  body10 = "<p>© 2017 MTC Kitchen<p>";
		String  body = body0 + body1 + body2 + body3 + body4 + body5 + body5_1 + body5_2 
				+ body6 + body7 + body8 + body9 + body10;
		mailService.sendMail(emailAddress, title, body);
		return new Response(true);
	}
}
