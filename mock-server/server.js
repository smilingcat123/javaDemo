import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import * as userController from './controllers/users';
import * as productController from './controllers/products';

const app = express();



app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());

//config routes
app.get("/users", userController.getUsers);
app.get("/users/:id", userController.getUser);
app.post("/users", userController.postUsers);
app.put("/users", userController.putUsers);
app.delete("/users/:id", userController.deleteUsers);

//GET /products
app.get("/products", productController.getProducts);
//GET /products/:name
app.get("/products/:pro_id", productController.getProduct);
//POST /products
app.post("/products", productController.postProducts);
//PUT /products
app.put("/products", productController.putProducts);
//DELETE /products
app.delete("/products/:pro_id", productController.deleteProducts);

app.listen(3000);
console.log("User server started");
console.log("Products server started");
