let Products = [
    {
        "pro_id": 1,
        "pro_src": "https://cdn6.bigcommerce.com/s-ih7nl5lym4/products/20967/images/36634/90257__28990.1466628351.190.285.jpg?c=2",
        "pro_title": "Tsukiji Masamoto White Steel 1 Yanagi knife 300mm (11.8\")  Junkou Honyaki",
        "pro_rating": "https://cdn6.bigcommerce.com/r-aac0322435b2c359692a75125d3d27d3de5e69cb/themes/ClassicNext/images/IcoRating5.png",
        "pro_price": 1200,
        "pro_total": 20
    },
    {
        "pro_id": 2,
        "pro_src": "https://cdn6.bigcommerce.com/s-ih7nl5lym4/products/23236/images/42112/90524__03837.1477409487.190.285.jpg?c=2",
        "pro_title": "Tsukiji Masamoto Honyaki White Steel Yanagi Knife 270mm (10.6\")",
        "pro_rating": "https://cdn6.bigcommerce.com/r-aac0322435b2c359692a75125d3d27d3de5e69cb/themes/ClassicNext/images/IcoRating4.png",
        "pro_price": 920,
        "pro_total": 10
    },
    {
        "pro_id": 3,
        "pro_src": "https://cdn6.bigcommerce.com/s-ih7nl5lym4/products/20970/images/37561/90524__05889.1466628412.190.285.jpg?c=2",
        "pro_title": "Tsukiji Masamoto White Steel 2 Honyaki Yanagi knife 240mm (9.4\";)",
        "pro_rating": "https://cdn6.bigcommerce.com/r-aac0322435b2c359692a75125d3d27d3de5e69cb/themes/ClassicNext/images/IcoRating3.png",
        "pro_price": 800,
        "pro_total": 13
    },
    {
        "pro_id": 4,
        "pro_src": "https://cdn6.bigcommerce.com/s-ih7nl5lym4/products/23235/images/42106/91759__22303.1477407985.190.285.jpg?c=2",
        "pro_title": "Tsukiji Masamoto Stain-Resistant Yanagi Knife 300mm (11.8\")",
        "pro_rating": "https://cdn6.bigcommerce.com/r-aac0322435b2c359692a75125d3d27d3de5e69cb/themes/ClassicNext/images/IcoRating3.png",
        "pro_price": 650,
        "pro_total": 3
    },
    {
        "pro_id": 5,
        "pro_src": "https://cdn6.bigcommerce.com/s-ih7nl5lym4/products/23234/images/42100/91759__28994.1477407714.190.285.jpg?c=2",
        "pro_title": "Tsukiji Masamoto Stain-Resistant Yanagi Knife 270mm (10.6\")",
        "pro_rating": "https://cdn6.bigcommerce.com/r-aac0322435b2c359692a75125d3d27d3de5e69cb/themes/ClassicNext/images/IcoRating3.png",
        "pro_price": 456,
        "pro_total": 9
    },
    {
        "pro_id": 6,
        "pro_src": "https://cdn6.bigcommerce.com/s-ih7nl5lym4/products/23271/images/42312/91653__46761.1478818656.190.285.jpg?c=2",
        "pro_title": "Nenox Gyuto Knife 240mm (9.4\") Autumn Gold Jigged Bone Handle with Wooden Cover",
        "pro_rating": "https://cdn6.bigcommerce.com/r-aac0322435b2c359692a75125d3d27d3de5e69cb/themes/ClassicNext/images/IcoRating3.png",
        "pro_price": 420,
        "pro_total": 9
    },
    {
        "pro_id": 7,
        "pro_src": "https://cdn6.bigcommerce.com/s-ih7nl5lym4/products/20970/images/37561/90524__05889.1466628412.190.285.jpg?c=2",
        "pro_title": "Tsukiji Masamoto White Steel 1 Yanagi Knife 300mm (11.8\") Left Handed",
        "pro_rating": "https://cdn6.bigcommerce.com/r-aac0322435b2c359692a75125d3d27d3de5e69cb/themes/ClassicNext/images/IcoRating5.png",
        "pro_price": 530,
        "pro_total": 0
    },
    {
        "pro_id": 8,
        "pro_src": "https://cdn6.bigcommerce.com/s-ih7nl5lym4/products/23296/images/42446/91455__50658.1479328906.190.285.jpg?c=2",
        "pro_title": "Tsukiji Masamoto White Steel 1 Yanagi Knife 300mm (11.8\")",
        "pro_rating": "https://cdn6.bigcommerce.com/r-aac0322435b2c359692a75125d3d27d3de5e69cb/themes/ClassicNext/images/IcoRating2.png",
        "pro_price": 356,
        "pro_total": 13
    },
    {
        "pro_id": 9,
        "pro_src": "https://cdn6.bigcommerce.com/s-ih7nl5lym4/products/21486/images/38428/90241__38949.1466628473.190.285.jpg?c=2",
        "pro_title": "Nenohi Kaede Mukimono Knife 180mm (7.1\")",
        "pro_rating": "https://cdn6.bigcommerce.com/r-aac0322435b2c359692a75125d3d27d3de5e69cb/themes/ClassicNext/images/IcoRating4.png",
        "pro_price": 420,
        "pro_total": 13
    },
    {
        "pro_id": 10,
        "pro_src": "https://cdn6.bigcommerce.com/s-ih7nl5lym4/products/22726/images/38321/98247__26238.1466628463.190.285.jpg?c=2",
        "pro_title": "Tsukiji Masamoto Blue Steel 1 (Aoko) Yanagi Knife 270mm (10.6\")",
        "pro_rating": "https://cdn6.bigcommerce.com/r-aac0322435b2c359692a75125d3d27d3de5e69cb/themes/ClassicNext/images/IcoRating5.png",
        "pro_price": 187,
        "pro_total": 13
    }

]

const getProducts = (req, res, next) => {
    res.json(Products);
};

const getProduct = (req, res, next) => {
    const {pro_id} = req.params;
    console.log(Products.find(p => p.pro_id == pro_id));
    res.json(Products.find(p => p.pro_id == pro_id));
};

const postProducts = (req, res, next) => {
    Products.push(req.body);
    res.json(Products);
};

const putProducts = (req, res, next) => {
    const { name } = req.body;
    products = products.filter(p => p.pro_id != pro_id);
    products.push(req.body);
    res.json(products);
}

const deleteProducts = (req, res, next) => {
    const { name } = req.params;
    products = products.filter(p => p.pro_id != pro_id);
    res.json(products);
}

export { getProducts, getProduct, postProducts, putProducts, deleteProducts};