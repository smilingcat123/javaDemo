let users = [
    {
        "login_email" : "super",
        "login_pass" : "super"
    }
]
const getUsers = (req, res, next) => {
    res.json(users);
};

const getUser = (req, res, next) => {
    const {login_email} = req.params;
    res.json(users.find(p => p.login_email == login_email));
};

const postUsers = (req, res, next) => {
    const {login_email, login_pass} = req.body;
    console.log(req.body);
    if(users.find((obj) => obj.login_email == login_email && obj.login_pass == login_pass )){
        return res.json(
            {
                success: true
            }
        )
    }else{
        return res.json(
            {
                success: false
            }
        )
    }
};

const putUsers = (req, res, next) => {
    const { login_email, login_pass } = req.body;
    console.log(req.body);
    //users = users.filter(p => p.login_email != login_email);
    users.push(req.body);
    res.json(users);
}

const deleteUsers = (req, res, next) => {
    const { login_email } = req.params;
    users = users.filter(p => p.login_email != login_email);
    res.json(users);
}

export { getUsers, getUser, postUsers, putUsers, deleteUsers};